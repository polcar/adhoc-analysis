
.. code-block:: shell 

	mkvirtualenv --python $(which python3) notebook
	workon notebook
	pip install -U pip setuptools
	pip3 install -r requirements.txt
	jupyter notebook --port 8000 --no-browser --ip=0.0.0.0
	jupyter notebook list	
